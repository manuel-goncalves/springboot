FROM openjdk:13
EXPOSE 5000
ADD ./target/books-0.0.1-SNAPSHOT.jar books-server.jar
ENTRYPOINT [ "java", "-jar", "/books-server.jar" ]