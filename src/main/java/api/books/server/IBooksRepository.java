package api.books.server;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import api.books.models.Books;

/**
 * IBooksRepository
 */
@RepositoryRestResource(path = "books")
public interface IBooksRepository extends PagingAndSortingRepository<Books, Long> {

}